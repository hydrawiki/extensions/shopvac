<?php
/**
 * Curse Inc.
 * ShopVac
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		ShopVac
 * @link		https://gitlab.com/hydrawiki
 *
**/

class SpecialShopVac extends SpecialPage {
	/**
	 * Main Constructor
	 *
	 * @access	public
	 * @return	void
	 */
	public function __construct() {
		parent::__construct(
			'ShopVac', // name
			'shopvac', // required user right
			true // display on Special:Specialpages
		);
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @param	string	Sub page passed in the URL.
	 * @return	void	[Outputs to screen]
	 */
	public function execute( $path ) {
		$this->template = new TemplateShopVac();
		$this->wgRequest = $this->getRequest();
		$this->wgUser    = $this->getUser();
		$this->output    = $this->getOutput();

		$this->checkPermissions();
		$debug = $this->wgRequest->getBool('debug');

		/* might change */
		$this->output->setPageTitle(wfMessage('shopvac'));
		$this->content = $this->template->scrapeForm($debug);

		$this->output->addModules(['ext.ShopVac']);
		$this->output->addHTML($this->content);

	}

	/**
	 * Return the group name for this special page.
	 *
	 * @access protected
	 * @return string
	 */
	protected function getGroupName() {
		return 'other'; //Change to display in a different category on Special:SpecialPages.
	}
}
