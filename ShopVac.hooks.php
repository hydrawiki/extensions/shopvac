<?php
/**
 * ShopVac
 * ShopVac Hooks
 *
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		ShopVac
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class ShopVacHooks {
	/**
	 * Adds additional menu items to Special:WikiSites.
	 *
	 * @access	public
	 * @param	array	HTML of extra menu options to add.
	 * @param	object	Wiki context to check.
	 * @return	boolean	True
	 */
	static function onWikiSitesMenuOptions(&$extraMenuOptions, $wiki) {
		global $wgUser;

		if ($wgUser->isAllowed('wiki_scrape') && $wiki->isInstalled()) {
			$shopvacPage		= Title::newFromText('Special:ShopVac');
			$shopvacURL			= $shopvacPage->getFullURL();

			$extraMenuOptions[] = "<a href='{$shopvacURL}?siteKey={$wiki->getSiteKey()}' title='".wfMessage('wikisites-scrape_into')->escaped()."'>".HydraCore::awesomeIcon('hand-lizard').wfMessage('wikisites-scrape_into')->escaped()."</a>";
		}

		return true;
	}
}
