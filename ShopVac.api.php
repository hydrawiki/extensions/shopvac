<?php
/**
 * ShopVac
 * ShopVac API
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		ShopVac
 * @link		https://gitlab.com/hydrawiki
 *
 **/

class ShopVacAPI extends ApiBase {
	/**
	 * API Initialized
	 *
	 * @var		boolean
	 */
	private $initialized = false;


	/**
	 * Initiates some needed classes.
	 *
	 * @access	public
	 * @return	void
	 */
	private function init() {
		if (!$this->initialized) {
			global $wgUser, $wgRequest;
			$this->wgUser		= $wgUser;
			$this->wgRequest	= $wgRequest;
			$this->initialized = true;
		}
	}

	/**
	 * Main Executor
	 *
	 * @access	public
	 * @return	void	[Outputs to screen]
	 */
	public function execute() {
		$this->init();

		$this->params = $this->extractRequestParams();

		if (!$this->getUser()->isLoggedIn()) {
			$this->dieWithError(['invaliduser', $this->params['do']]);
		}

		switch ($this->params['do']) {
			case 'getWikiInfoSrc':
				$response = $this->getWikiInfoSrc();
				break;
			case 'getWikiInfoDest':
				$response = $this->getWikiInfoDest();
				break;
			default:
				$this->dieWithError(['invaliddo', $this->params['do']]);
				break;
		}

		foreach ($response as $key => $value) {
			$this->getResult()->addValue(null, $key, $value);
		}
	}

	/**
	 * Requirements for API call parameters.
	 *
	 * @access	public
	 * @return	array	Merged array of parameter requirements.
	 */
	public function getAllowedParams() {
		return [
			'do' => [
				ApiBase::PARAM_TYPE		=> 'string',
				ApiBase::PARAM_REQUIRED => true
			],
			'domain' => [
				ApiBase::PARAM_TYPE		=> 'string',
				ApiBase::PARAM_REQUIRED => false
			]
		];
	}

	/**
	 * Descriptions for API call parameters.
	 *
	 * @access	public
	 * @return	array	Merged array of parameter descriptions.
	 */
	public function getParamDescription() {
		return [
			'do'		=> 'Action to take',
			'domain'	=> 'Domain to pull information from',
		];
	}

	/**
	 * Merge API errors into the mediawiki error array.
	 *
	 * @access	public
	 * @return	array
	 */
	public function getPossibleErrors() {
		return array_merge(
			parent::getPossibleErrors(),
			[
				[
					'code' => 'NoDomain',
					'info' => 'No domain was passed.'
				],
				[
					'code' => 'RemoteApiFail',
					'info' => 'We were unable to call the remote wikis api.'
				],
				[
					'code' => 'RemoteApiInvalidJSON',
					'info' => 'Remote API returned invalid JSON.'
				],
				[
					'code' => 'DestinationWikiNotFound',
					'info' => 'Destination Wiki was not found in our system.'
				],
				[
					'code' => 'DestinationWikiNotInstalled',
					'info' => 'Destination Wiki was not installed. Please install first.'
				]
			]
		);
	}

	/**
	 * Get preflight data for source wiki.
	 *
	 * @access	public
	 * @return	boolean Success
	 */
	public function getWikiInfoSrc() {
		if (empty($this->params['domain'])) {
			return ['success' => false, 'error' => true, 'code' => 'NoDomain'];
		}

		$apiPoint = "/api.php?action=query&meta=siteinfo&siprop=general|namespaces&format=json&formatversion=2";

		$srcWiki = trim(trim(str_replace(['http://', 'https://'], '', $this->params['domain']), '\\'));
		$srcWikiUrl = "https://" . $srcWiki . $apiPoint;

		$responseRaw = Http::get(
			$srcWikiUrl,
			[
				'sslVerifyCert' => false,
				'sslVerifyHost' => false
			]
		);

		$useHTTP = false;

		if (!$responseRaw) {
			//Try insecure.
			$srcWikiUrl = "http://" . $srcWiki . $apiPoint;
			$responseRaw = Http::get($srcWikiUrl);
			if (!$responseRaw) {
				return ['success' => false, 'error' => true, 'code' => 'RemoteApiFail'];
			}
			$useHTTP = true;
		}

		$responseJson = json_decode($responseRaw, 1);
		if (!$responseJson) {
			return ['success' => false, 'error' => true, 'code' => 'RemoteApiInvalidJSON'];
		}

		$response = $responseJson['query']['general'];
		$response['namespaces'] = $responseJson['query']['namespaces'];
		$response['useHTTP'] = $useHTTP;
		return ['success' => true, 'data' => $response];
	}

	/**
	 * Get preflight data for source wiki.
	 *
	 * @access	public
	 * @return	boolean Success
	 */
	public function getWikiInfoDest() {
		if (empty($this->params['domain'])) {
			return ['success' => false, 'error' => true, 'code' => 'NoDomain'];
		}

		$destWiki = $this->params['domain'];
		$wiki = \DynamicSettings\Wiki::loadFromDomain($destWiki);

		if (!$wiki) {
			return ['success' => false, 'error' => true, 'code' => 'DestinationWikiNotFound'];
		}

		if (!$wiki->isInstalled()) {
			return ['success' => false, 'error' => true, 'code' => 'DestinationWikiNotInstalled'];
		}

		$return = [
			'name' => $wiki->getName(),
			'sitekey' => $wiki->getSiteKey(),
			'domain'  => $wiki->getDomain()
		];

		return ['success' => true, 'data' => $return];
	}

	/**
	 * Get version of this API Extension.
	 *
	 * @access	public
	 * @return	string	API Extension Version
	 */
	public function getVersion() {
		return '1.0';
	}

	/**
	 * Return a ApiFormatJson format object.
	 *
	 * @access	public
	 * @return	object	ApiFormatJson
	 */
	public function getCustomPrinter() {
		return $this->getMain()->createPrinterByName('json');
	}
}
