<?php
/**
 * Curse Inc.
 * ShopVac
 * ShopVac Aliases
 *
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		ShopVac
 * @link		https://gitlab.com/hydrawiki
 *
**/

$specialPageAliases = [];

/** English (English) */
$specialPageAliases['en'] = [
	'ShopVac' => ['ShopVac']
];
