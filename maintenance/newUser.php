<?php
require_once(dirname(__DIR__, 3) . '/maintenance/Maintenance.php');

/**
 * Maintenance script that creates a user from a name, ignoring users that already exist.
 *
 * @ingroup Maintenance
 */
class UserCreator extends Maintenance {

	public function __construct() {
		parent::__construct();
		$this->addDescription("Create a valid user from a username");
		$this->addArg( 'username', 'Username(s) to create (comma seperated)', false );
	}

	public function execute() {
		$usernames = $this->getArg();

		$users = explode( ",", $usernames );
		foreach ($users as $user) {
			$make = User::createNew($user);
			if ( $make ) {
				$this->output( "Create a new user for {$user}\n" );
			} else {
				$this->output( "User {$user} already exists, or is not a valid username.\n" );
			}
		}
	}
}

$maintClass = UserCreator::class;
require_once RUN_MAINTENANCE_IF_MAIN;
