const workerpool = require('workerpool'),
	execSync = require('child_process').execSync,
	exec = require('child_process').exec,
	request = require('retry-request', {request: require('request')}),
	fs = require('fs-extra'),
	ansi = require('ansi-escape-sequences')
	;

var shellCommand = (importcmd, pageid, timeout, extra = []) => {
	return new Promise((resolve, reject) => {
		exec(importcmd, {timeout: timeout}, (err, stdout, stderr) => {
			resolve({
				output: stdout,
				pageid: pageid,
				error: err,
				importcmd: importcmd,
				extra: extra
			});
		});
	});
}

var jsonRequest = (opts) => {
	return new Promise((resolve, reject) => {
		request(opts, (err, resp, body) => {
			data = JSON.parse(body);
			resolve({
				err: err,
				data: data
			});
		});
	});
}

var importImage = (input, config) => {
	return new Promise((resolve, reject) => {
		// verify existance of directory.
		var dir = config.basePath + "/images/" + Math.floor(new Date().getTime() + (Math.random() * 10000));
		fs.mkdirpSync(dir);
		var file = dir + "/" + input.name;

		var ret = {output: ""};
		var r = request(input.url)
			.on('response', function (response) {
				var fileStream = fs.createWriteStream(file);
				fileStream.on('error', function (err) {
					ret.output = (ansi.format("Image Download:", ['red', 'bg-black']) + " File Creation Error for " + file);
					if (err) {
						throw new Error(err);
					}
					ret.failedImages = file;
					resolve(ret);
				});
				r.pipe(fileStream);
				r.on('end', function(){
					var cmd = `php ${config.hcMaintPath}/hostHelper.php ${config.destWiki} ${config.maintPath}/importImages.php ${dir} --overwrite`;
					if (config.remoteImageApi) {
						cmd += ` --source-wiki-url="${config.remoteImageApi}"`;
					}
					
					exec(cmd, (error, stdout, stderr) => {
						if (error) {
							ret.output = (ansi.format("Image Import:", ['red', 'bg-black']) + " Import Error for " + input.name);
							if (error.toString().includes("was not found or is not cached out to disk!")) {
								// this is a pretty clear "the wiki importing to doesn't exist error"
								ret.output = ("It appears the Wiki we are trying to import into does not exist.\n\n" + error);
							}
							throw new Error(error);
							resolve(ret);
						}
						ret.output = (ansi.format("Image Import:", ['green', 'bg-black']) + " Import Finished for " + input.name);
						fs.remove(dir, (err) => {
							if (err) {
								throw new Error(err);
							}
							resolve(ret);
						});
					});
				});
			})
			.on('error', function (err) {
				ret.output += "\n" + (ansi.format("Image Download:", ['red', 'bg-black']) + " Download Error for " + file);
				if (err) {
					throw new Error(err);
				}
				ret.failedImages = file;
				resolve(ret);
			});
	});
};

workerpool.worker({
	shellCommand: shellCommand,
	importImage: importImage,
	jsonRequest: jsonRequest
});