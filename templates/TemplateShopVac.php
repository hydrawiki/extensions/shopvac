<?php
/**
 * Curse Inc.
 * ShopVac
 *
 * @author		Cameron Chunn
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		ShopVac
 * @link		https://gitlab.com/hydrawiki
 *
**/

class TemplateShopVac {
	/**
	 * Scraper Form
	 *
	 * @access	public
	 * @return	string	HTML
	 */
	public function scrapeForm($debug) {
		global $wgRequest, $wgUser;

		$url = Title::newFromText('Special:WikiSites')->getPrefixedUrl() . "?section=scrape";

		$srcWiki = "";
		$destWiki = "";

		if ($debug) {
			// prefill this crap for debugging. Sick of typing it.
			$srcWiki = "miley-cyrus.wikia.com";
			$destWiki = "mileycyrus.gamepedia.wiki";
		}

		$siteKey = $wgRequest->getText('siteKey');
		if ($siteKey) {
			$wiki = \DynamicSettings\Wiki::loadFromHash($siteKey);
			if ($wiki) {
				$destWiki = $wiki->getDomain();
			}
		}

		$html = "
			<div id=\"preflightPanel\">
				<h2>".wfMessage("preflight")->plain()."</h2>
				<p>".wfMessage("shopvac_preflight_1")->plain()."</p>
				<p>".wfMessage("shopvac_preflight_2")->plain()."</p>
				<input type=\"text\" placeholder=\"somewiki.wikia.com\" id=\"srcWiki\" value=\"${srcWiki}\"> =>
				<input type=\"text\" placeholder=\"somewiki.gamepedia.com\" id=\"destWiki\" value=\"${destWiki}\">
				<button id=\"preflightRun\">".wfMessage("run_preflight")->plain()."</button>
				<pre id=\"preflightStatus\">".wfMessage("please_run_preflight")->plain()."</pre>
			</div>
			<div class=\"disableForPreflight\" style=\"display: none;\">
				<div id=\"srcWikiPanel\">
					<h2>".wfMessage("source_wiki_information")->plain()."</h2>
						<pre id=\"srcWikiJSON\">".wfMessage("please_run_preflight")->plain().".</pre>
						<h3>".wfMessage("data_to_collect")->plain()."</h3>
						<input name=\"collectData\" type=\"radio\" value=\"both\" checked=\"checked\">".wfMessage("collect_both")->plain()."<br />
						<input name=\"collectData\" type=\"radio\" value=\"content\">".wfMessage("collect_content")->plain()."<br />
						<input name=\"collectData\" type=\"radio\" value=\"images\">".wfMessage("collect_images")->plain()."<br />
						<input name=\"collectData\" type=\"radio\" value=\"images_only\">".wfMessage("collect_images_only")->plain()."<br />
						<div id=\"namespaceSelector\">
							<h3>".wfMessage("namespaces_to_scrape")->plain()."</h3>
							<p>".wfMessage("only_selected_will_be_scraped")->plain()."</p>
							<div id=\"namespaceCheckboxes\">
							".wfMessage("run_to_load_namespaces")->plain()."
							</div>
						</div>
						<div id=\"dateRangeSelector\">
							<h3>".wfMessage("revision_date_settings")->plain()."</h3>
							<p>".wfMessage("revision_date_settings_sub")->plain()."</p>
							<p>
							<label for=\"minRevision\">".wfMessage("min_rev_date")->plain()."</label>
							<input type=\"text\" id=\"minRevision\" placeholder=\"YYYY-MM-DD\"/>
							<small>".wfMessage("min_rev_date_desc")->plain()."</small>
							</p>
							<p>
							<label for=\"maxRevision\">".wfMessage("max_rev_date")->plain()."</label>
							<input type=\"text\" id=\"maxRevision\" placeholder=\"YYYY-MM-DD\"/>
							<small>".wfMessage("max_rev_date_desc")->plain()."</small>
							</p>
						</div>
					<h2>".wfMessage("destination_wiki_information")->plain()."</h2>
						<h3>".wfMessage("local_wiki_import_into")->plain()."</h3>
						<pre id=\"destWikiJSON\">".wfMessage("please_run_preflight")->plain()."</pre>
				</div>
				<h2>".wfMessage("generated_scraper_info")->plain()."</h2>
				<p>".wfMessage("generated_scraper_info_tagline")->plain()."</p>
				<form method=\"POST\" action=\"/${url}\">
						<input id=\"siteKey\" name=\"siteKey\" type=\"hidden\">
						<textarea id=\"scraperArgs\" name=\"scraperArgs\"></textarea><br />
						<h3>".wfMessage("run_scrape")->plain()."</h3>
						<button type=\"submit\" id=\"runBtn\" title=\"scrape\" disabled><span class=\"fa fa-hand-lizard\"></span>".wfMessage("spawn_scrape_job")->plain()."</button>
						<span id=\"runMsg\">".wfMessage("cant_run_before_preflight")->plain()."</span>
				</form>
			</div>";

		return $html;
	}
}
