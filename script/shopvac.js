/*jshint esversion: 6 */
const request = require('retry-request', {request: require('request')}),
	_ = require('underscore'),
	spawn = require('child_process').spawn,
	fs = require('fs-extra'),
	ansi = require('ansi-escape-sequences'),
	commandLineArgs = require('command-line-args'),
	Queue = require('better-queue'),
	getUsage = require('command-line-usage'),
	redis = require('redis'),
	Table = require('easy-table'),
	os = require('os'),
	moment = require('moment'),
	minimatch = require("minimatch"),
	parseString = require('xml2js').parseString,
	xmlBuilder = require('xml2js').Builder,
	workerpool = require('workerpool'),
	engine = require('php-parser');

let importPool = workerpool.pool(__dirname + '/importWorker.js');
iwd = 0; // imports outside of workerpool tracking

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
var start = new Date();

//  Add some basic error handling to hopefully make the event runner happy.
process.on('uncaughtException', function (err) {
	// Handle any of these and make sure the exit code is 1.
	error = `
  ___ _ __ _ __ ___  _ __
 / _ \\ '__| '__/ _ \\| '__|
|  __/ |  | | | (_) | |
 \\___|_|  |_|  \\___/|_|`;
	console.log(ansi.format(error, 'red') + ansi.format(" of the uncaught, fatal variety",'gray') + "\n\n");
	console.error(err);
	process.exit(1);
});
process.on('exit', (code) => {
	console.log(`Exiting with code: ${code}`);
});

/***
 *       _____      _
 *      / ____|    | |
 *     | (___   ___| |_ _   _ _ __
 *      \___ \ / _ \ __| | | | '_ \
 *      ____) |  __/ |_| |_| | |_) |
 *     |_____/ \___|\__|\__,_| .__/
 *                           | |
 *                           |_|
 */

const optionDefinitions = [
	{ name: 'help', alias: 'h', type: Boolean, defaultValue: false, description: 'Shows this message' },
	{
		name: 'srcWiki',
		alias: 's',
		type: String,
		description: 'The Source Wiki',
		typeLabel: '[underline]{somewiki.wikia.com}',
		group: 'export'
	}, {
		name: 'destWiki',
		alias: 'd',
		type: String,
		description: 'The Destination Wiki',
		typeLabel: '[underline]{somewiki.gamepedia.com}',
		group: 'export'
	}, {
		name: 'ignoreNS',
		alias: 'i',
		type: Number,
		multiple: true,
		description: 'Namespace to ignore while scraping. Takes Priority over onlyNS.',
		typeLabel: '[underline]{110} [underline]{111} ...',
		group: 'export',
		defaultValue: []
	}, {
		name: 'onlyNS',
		alias: 'o',
		type: Number,
		multiple: true,
		description: 'Only export these namespaces. Otherwise defaults to ALL.',
		typeLabel: '[underline]{110} [underline]{111} ...',
		group: 'export',
		defaultValue: []
	}, {
		name: 'skipContent',
		type: Boolean,
		defaultValue: false,
		group: 'export',
		description: 'Set flag to skip the scraping of page content.'
	}, {
		name: 'purgeRevisions',
		type: Boolean,
		defaultValue: false,
		group: 'export',
		description: 'This will purge the revision history and set all pages as created at time of run by the "Maintenance Script" user. '
	}, {
		name: 'minRevisionDate',
		alias: 'b',
		type: String,
		defaultValue: false,
		group: 'export',
		typeLabel: '[underline]{"2076-07-04"}',
		description: 'Dont import any revisions before this date'
	}, {
		name: 'maxRevisionDate',
		alias: 'e',
		type: String,
		defaultValue: false,
		group: 'export',
		typeLabel: '[underline]{"2076-10-23"}',
		description: 'Dont import any revisions after this date'
	}, {
		name: 'maxExportPool',
		type: Number,
		defaultValue: 500,
		group: 'export',
		typeLabel: '[underline]{500}',
		description: 'Maximum number of concurrent exports to run (may be helpful on slow servers).'
	}, {
		name: 'titlesPerRequest',
		type: Number,
		defaultValue: 500,
		group: 'export',
		typeLabel: '[underline]{500}',
		description: 'Maximum number of titles to pull in a single API request (may be helpful on slow servers).'
	}, {
		name: 'skipImages',
		type: Boolean,
		defaultValue: false,
		group: 'export',
		description: 'Set flag to skip the scraping of images.'
	}, {
		name: 'importImagesUsingSourceAPI',
		type: Boolean,
		defaultValue: false,
		group: 'export',
		description: 'Set flag to pass the remote API to the import image script using source-wiki-url.'
	}, {
		name: 'skipXMLDump',
		type: Boolean,
		defaultValue: false,
		group: 'export',
		description: 'Set flag to skip dumping. Only useful for testing, as it will cause failure if previos dumps dont exist.'
	}, {
		name: 'aggressiveGC',
		type: Boolean,
		defaultValue: false,
		group: 'dev',
		description: 'Set flag to force agressive GC. Requires node to have --expose_gc.'
	}, {
		name: 'debug',
		type: Boolean,
		defaultValue: false,
		group: 'dev',
		description: 'Set flag to dump extra info.'
	},{
		name: 'debugXML',
		type: Boolean,
		defaultValue: false,
		group: 'dev',
		description: 'Set flag to dump xml out as its importing.'
	},{
		name: 'testRunOnly',
		type: Boolean,
		defaultValue: false,
		group: 'dev',
		description: 'Set flag to skip the actual import of the XML.'
	},{
		name: 'debugAndDie',
		type: Boolean,
		defaultValue: false,
		group: 'dev',
		description: 'Set flag to dump information passed in and then exit.'
	}, {
		name: 'useHTTP',
		type: Boolean,
		defaultValue: false,
		group: 'dev',
		description: 'If passed, use http:// instead of https://.'
	}, {
		name: 'cleanBeforeStart',
		type: Boolean,
		defaultValue: false,
		group: 'dev',
		description: 'If passed, will remove any current dumps that may exist on the worker. Use with caution!'
	}, {
		name: 'redisKey',
		type: String,
		defaultValue: false,
		group: 'redis',
		typeLabel: '[underline]{Some:redis:key}',
		description: 'Key to stream output to. If this is set, redis streaming will be enabled and we will attempt to output all console output into redis as well.'
	}, {
		name: 'redisServer',
		type: String,
		defaultValue: '127.0.0.1',
		group: 'redis',
		typeLabel: '[underline]{127.0.0.1}',
		description: 'IP/URL of Redis Server.'
	}, {
		name: 'redisPort',
		type: Number,
		defaultValue: 6379,
		group: 'redis',
		typeLabel: '[underline]{6379}',
		description: 'Port of Redis Server.'
	}, {
		name: 'redisPrefix',
		type: String,
		defaultValue: 'Hydra:',
		group: 'redis',
		typeLabel: '[underline]{Hydra:}',
		description: 'A string used to prefix all used keys. MediaWiki uses one, so we use one.'
	},
];
const options = commandLineArgs(optionDefinitions);

var redisClient = false;
if (options.redis.redisKey !== null && options.redis.redisKey !== false) {
	/* We have everything we need to stream into redis, at least in theory */
	redisClient = redis.createClient({
		host: options.redis.redisServer,
		port: options.redis.redisPort,
		prefix: options.redis.redisPrefix
	});

	redisClient.on('error', function (err) {
		jobErrorOutput('REDIS ERROR', err);
	});
}

// Output handler
var jobOutput = (content) => {
	console.log(content);
	if (redisClient) {
		redisClient.append(options.redis.redisKey, content + "\n");
	}
};

var jobErrorOutput = (content) => {
	try {
		content = ansi.format(content, 'red');
		jobOutput(content);
	} catch (e) {
		jobOutput(ansi.format("ERROR:", 'red'));
		jobOutput(content);
	}
};

var jobWarningOutput = (content) => {
	jobOutput(ansi.format(`WARNING: ${content}`, 'yellow'));
}

var jobDebugOutput = (content) => {
	if (options.dev.debug) {
		jobOutput(content);
	}
};

var jobDebugXMLOutput = (content) => {
	if (options.dev.debugXML) {
		jobOutput(content);
	}
}

var otherScriptDebugOutput = (content) => {
	lines = content.split("\n");
	frontSpacer = "	| ";
	newlines = [];
	for (var x in lines) {
		if (lines[x].length) {
			newlines.push(lines[x]);
		}
	}
	content = frontSpacer + newlines.join("\n"+frontSpacer);
	jobDebugOutput(content);
}

var lastGC = moment();

var gc = () => {
	if (options.dev.aggressiveGC) {
		var now = moment();
		var diff = moment.duration(now.diff(lastGC));
		if ( diff.asSeconds() > 5 ) {
			lastGC = now;
			if (global.gc) {
				jobOutput('Forcing Garbage Collection to free up memory.')
				global.gc();
			} else {
				jobWarningOutput('Attempted to force garbage colection, but node was not ran with --expose_cg. We may run out of memory on a large operation.')
			}
		}
	}
}


/**
 *  Handle command validation & help message
 */
var showHelp = (error) => {
	var sections = [
		{ header: 'exportr Options', optionList: optionDefinitions, group: 'export' },
		{ header: 'Redis Output Streaming', optionList: optionDefinitions, group: 'redis' },
		{ header: 'Misc', optionList: optionDefinitions, group: '_none' },
		{ header: 'Dev', optionList: optionDefinitions, group: 'dev' }
	];
	if (typeof error == "string") {
		sections.splice(0, 0, {
			header: 'Error Running ShopVac',
			content: error
		})
	}
	const usage = getUsage(sections);
	jobOutput(usage);
	process.exit();
}


const ShopVacHeader = ansi.format(require('./assets/logo'), 'red');
jobOutput("\n\n" + ShopVacHeader + "\n");
jobOutput("Running on " + ansi.format(os.hostname(), 'cyan') + "\n");

if (options._all.help == true) { showHelp(); }


jobDebugOutput(options);


if (options.dev.debugAndDie) {
	jobOutput(options);
	process.exit();
}

if (typeof options.export.srcWiki === "undefined") { showHelp('You must define a Source Wiki to export.'); }
if (typeof options.export.destWiki === "undefined") { showHelp('You must define a Destination Wiki to export.'); }
if (options.export.ignoreNS.length) {
	for (var x in options.export.ignoreNS) {
		if (isNaN(options.export.ignoreNS[x])) { showHelp('Invalid argument passed for [bold]{--ignoreNS}'); }
	};
}
for (var x in options.export.onlyNS) {
	if (isNaN(options.export.onlyNS[x])) { showHelp('Invalid argument passed for [bold]{--onlyNS}'); }
};

if (options.export.skipImages && options.export.skipContent) { showHelp('We have nothing to do.'); }


jobOutput('ShopVac running with following options:');
jobOutput('  + Source Wiki: 		' + options.export.srcWiki);
jobOutput('  + Destination Wiki:		' + options.export.destWiki);
if (options.export.ignoreNS.length) {
	jobOutput('  + Ignore Namespace(s):	' + options.export.ignoreNS.join());
}
if (options.export.onlyNS.length) {
	jobOutput('  + Only export Namespace(s):	' + options.export.onlyNS.join());
}

jobOutput("\n");


console.time("ShopVac Process");

let opt = {};

// Set Options
opt.srcWiki = options.export.srcWiki;
opt.destWiki = options.export.destWiki;
opt.urlProtocol = options.dev.useHTTP ? "http" : "https";
opt.remoteImageApi = options.export.importImagesUsingSourceAPI ? (opt.urlProtocol + "://" + options.export.srcWiki + "/") : false;

// Dump Paths
var d = new Date();
var dumpName = opt.srcWiki + "-" + opt.destWiki + "-" + d.getUTCDay() + "-" + (d.getUTCMonth() + 1) + "-" + d.getUTCFullYear();

// Parse php localsettings to extract all variables.
var localSettingsPath = `${__dirname}`.replace('extensions/ShopVac/script', 'LocalSettings.php');
var parser = new engine({ parser: { extractDoc: false, php7: true }, ast: { withPositions: false } });
var phpConfig = fs.readFileSync( localSettingsPath );
var parsed = parser.parseCode(phpConfig);
var phpVars = [];
for (var x in parsed['children']) {
	var c = parsed['children'][x];
	if (c['kind'] == 'expressionstatement' && c['expression']['kind'] == 'assign' && c['expression']['left']['kind'] == 'variable') {
		var e = c['expression'];
		phpVars[e['left']['name']] = e['right']['value'];
	}
}

opt.dumpBasePath = phpVars['wgHydraCacheBasePath'] ? phpVars['wgHydraCacheBasePath'] : '/media/hydra-cache'
opt.dumpPath = `${opt.dumpBasePath}/shopvac/`
opt.basePath = `${opt.dumpPath}${dumpName}`;

// Paths for Maint Scripts that have to be ran.
opt.maintPath = `${__dirname}`.replace('extensions/ShopVac/script', 'maintenance');
opt.hcMaintPath = `${__dirname}`.replace('ShopVac/script', 'HydraCore/maintenance');
opt.haMaintPath = `${__dirname}`.replace('ShopVac/script', 'HydraAuth/maintenance');
opt.dsMaintPath = `${__dirname}`.replace('ShopVac/script', 'DynamicSettings/maintenance');
opt.svMaintPath = `${__dirname}`.replace('ShopVac/script', 'ShopVac/maintenance');

if (options.dev.cleanBeforeStart) {
	jobOutput('Wiping away the dumps directory');
	fs.removeSync(opt.dumpPath);
	fs.emptyDirSync(opt.dumpPath);
}

// create working directories.
fs.mkdirpSync(opt.basePath);
fs.emptyDirSync(opt.basePath + "/processed");
fs.mkdirpSync(opt.basePath + "/processed");
fs.emptyDirSync(opt.basePath + "/logs");
fs.mkdirpSync(opt.basePath + "/logs");
fs.mkdirpSync(opt.basePath + "/scripts");
fs.mkdirpSync(opt.basePath + "/images");
fs.mkdirpSync(opt.basePath + "/titles");

var fatalDontDelete = false;

var purgeDump = (cb) => {
	try {
		jobOutput('Removing dump files to save space on worker: ' + opt.basePath);
		fs.removeSync(opt.basePath);
	} catch (e) {
		jobErrorOutput("We were unable to cleanup the dump. Oops.");
	}
	cb();
};

var hardFail = (msg) => {
	jobOutput('\nWe have encountered an error that we can\'t recover from:');
	jobOutput(msg);
	jobOutput('\n\n[process failed]\n');

	purgeDump(() => {
		jobOutput("\n\n");
		console.timeEnd("ShopVac Process");
		jobOutput("\n\n");
		process.exit(1);
	});
};

/**
 * Keep track of current actions so we can have a status message for users
 */

/***
 *       _____ _        _               _    _                 _ _
 *      / ____| |      | |             | |  | |               | | |
 *     | (___ | |_ __ _| |_ _   _ ___  | |__| | __ _ _ __   __| | | ___ _ __
 *      \___ \| __/ _` | __| | | / __| |  __  |/ _` | '_ \ / _` | |/ _ \ '__|
 *      ____) | || (_| | |_| |_| \__ \ | |  | | (_| | | | | (_| | |  __/ |
 *     |_____/ \__\__,_|\__|\__,_|___/ |_|  |_|\__,_|_| |_|\__,_|_|\___|_|
 *
 *
 */

var status = {
	'Scraping Namespaces': 0,
	'Processing Namespaces': 0,
	'Importing Namespaces': 0,
	'Completed Namespaces': 0,
	'Failed Namespaces': 0,
	'Total Namespaces': 0,
	'Total Titles': 0,
	'Completed Titles': 0,
	'Failed Titles': 0,
	'Retrying Imports': 'Idle',
	'Image Status': 'Idle',
	'Total Images': 0,
	'Complete Images': 0,
	'Failed Images': 0
};

var statusHasChanges = false;
var discoveredExtensions = [];
var perfStats = {
	"import": [],
	"export": [],
	"process": []
};

var findAvg = (total, amount, index, array) => {
	total += amount;
	if (index === array.length - 1) {
		return total / array.length;
	} else {
		return total;
	}
};

poolShowedDone = false;
var handleMainStatus = () => {
	var running = new Date() - start;
		running = running / 1000;

	var statusString = ansi.format('Current Status: ', 'bold');
	var stats = [];
	stats.push(ansi.format("Timestamp", 'cyan') + ": " + moment().format());
	stats.push(ansi.format("Run Time", 'cyan') + ": " + running + "s");
	for (x in status) {
		if (status[x] !== 0) {
			stats.push(ansi.format(x, 'cyan') + ": " + status[x]);
		}
	}
	if (importPool) {
		var cpoolStatus = importPool.stats();
		stats.push(ansi.format("Import Worker", 'cyan') + ": " + JSON.stringify(cpoolStatus));
	}

	if (iwd) {
		stats.push(ansi.format("Main Thread Process", 'cyan') + ": " + iwd);
	}


	importPoolFinished = false;
	if (importPool && cpoolStatus.pendingTasks == 0 && cpoolStatus.activeTasks == 0 && iwd == 0) {
		if (!poolShowedDone) {
			stats.push(ansi.format('Pool Status', 'cyan') + ": Worker Pool Appears Empty. Setting countdown timer to force finish.");
			poolShowedDone = new Date() / 1000;
		} else {
			now = new Date() / 1000;
			if (now - poolShowedDone > 600) {
				stats.push(ansi.format('Pool Status', 'cyan') + ": Worker Pool Empty for 600 seconds. Setting Done.");
				importPoolFinished = true;
			} else {
				stats.push(ansi.format('Pool Status', 'cyan') + `: Worker Pool Empty for ${Math.floor(now - poolShowedDone)}/600 seconds.`);
			}
		}
	} else {
		stats.push(ansi.format('Pool Status', 'cyan') + ": Worker Pool has work.");
		poolShowedDone = false;
	}


	try {
		var used = process.memoryUsage().heapUsed / 1024 / 1024;
		stats.push(ansi.format("Memory Usage", 'cyan') + ": " + `${Math.round(used * 100) / 100} MB`);
	} catch(e) {
		// failed. Oh well.
	}

	jobOutput("\n" + statusString + "\n -- " + stats.join("\n -- ") + "\n");
	if (statusHasChanges &&
		(status['Total Namespaces'] == (status['Completed Namespaces'] + status['Failed Namespaces']) &&
		(status['Image Status'] == "Complete" || status['Image Status'] == "Idle")
		) || importPoolFinished) {
		jobOutput('PROCESS DONE!');
		importPool.terminate();
		fireProcessComplete();
		clearInterval(statusInterval);
		setInterval(handleSimpleStatus, 15000);
		processComplete = true;
	}
};

var statusInterval = setInterval(handleMainStatus, 15000);

var handleSimpleStatus = () => {
	var running = new Date() - start;
	running = running / 1000;

	var statusString = ansi.format('Current Status: ', 'bold');
	var stats = [];
	stats.push(ansi.format("Timestamp", 'cyan') + ": " + moment().format());
	stats.push(ansi.format("Run Time", 'cyan') + ": " + running + "s");
	stats.push(ansi.format("Status", 'cyan') + ": Running Final Processing");

	jobOutput("\n" + statusString + "\n -- " + stats.join("\n -- ") + "\n");
}

/***
 *      _____                               ______ _       _     _
 *     |  __ \                             |  ____(_)     (_)   | |
 *     | |__) | __ ___   ___ ___  ___ ___  | |__   _ _ __  _ ___| |__   ___ _ __ ___
 *     |  ___/ '__/ _ \ / __/ _ \/ __/ __| |  __| | | '_ \| / __| '_ \ / _ \ '__/ __|
 *     | |   | | | (_) | (_|  __/\__ \__ \ | |    | | | | | \__ \ | | |  __/ |  \__ \
 *     |_|   |_|  \___/ \___\___||___/___/ |_|    |_|_| |_|_|___/_| |_|\___|_|  |___/
 *
 *
 */

var runPostScripts = (cb) => {
	let script = [
		`${opt.hcMaintPath}/hostHelper.php`,
		`${opt.destWiki}`,
		`${opt.maintPath}/rebuildrecentchanges.php`
	];
	runSpanwedPHPProcess('Rebuild Recent Changes', script, ()=>{
		let script = [
			`${opt.hcMaintPath}/hostHelper.php`,
			`${opt.destWiki}`,
			`${opt.dsMaintPath}/refreshLinksChunked.php`
		];
		runSpanwedPHPProcess('Link Refresh', script, ()=>{
			cb();
		});
	});
}

let runSpanwedPHPProcess = (name, args,  done) => {
	jobOutput(`\n\nRunning ${name}\n\n`);
	const task = spawn('php',args);
	task.stdout.on('data', (data) => {
		jobOutput(data.toString());
	});
	task.stderr.on('data', (data) => {
		jobErrorOutput(data.toString());
	});
	task.on('close', (code) => {
		jobOutput(`${name} exited with code ${code}\n\n`);
		done();
	});
}

var domainStats = {};

var getStatsForDomain = (domain, cb) => {
	if (domainStats[domain]) {
		cb(domainStats[domain]);
	} else {
		var Url = opt.urlProtocol + "://" + domain + "/api.php?action=query&meta=siteinfo&siprop=statistics&format=json";
		jobOutput("Pulling stats for " + domain);
		var call = request({ method: "GET", uri: Url, json: true }, (error, response, body) => {
			if (error) {
				jobOutput('Error gathering stats for ' + domain + '.');
				cb(false);
			} else {
				domainStats[domain] = body;
				cb(body);
			}
		});
	}
}

var sanityCheck = (cb) => {
	jobOutput("\n\nFiring up sanity check");
	getStatsForDomain(opt.srcWiki, (srcStats) => {
		getStatsForDomain(opt.destWiki, (destStats) => {
			if (!srcStats || !destStats) {
				jobOutput("Can't compare for sanity check. Oh well...");
			} else {
				try {
					var ss = srcStats.query.statistics;
					var ds = destStats.query.statistics;
					ss.wiki = opt.srcWiki;
					ds.wiki = opt.destWiki;
					var t = new Table;
					var data = [ss, ds];
					data.forEach(function (w) {
						t.cell('Wiki', w.wiki);
						t.cell('pages', w.pages);
						t.cell('articles', w.articles);
						t.cell('edits', w.edits);
						t.cell('images', w.images);
						t.newRow();
					});
					jobOutput("\n\n" + t.toString());
					jobOutput("Please check the numbers above for any unexpected discrepency, and take proper action if any are found.\n\n\n");
				} catch (err) {
					jobErrorOutput('Failed collecting stats for Sanity Check. Sad Face Emoji.');
				}
			}
			cb();
		});
	});
}

var postImportCount = 0;
var postImportErrorCheck = (cb) => {
	var error_count = retryPageImports.length;
	if (error_count) {
		status['Retrying Imports'] = error_count;
		jobErrorOutput(`\n\nA total of ${error_count} pages failed to import under the 10 minute mark.`);
		jobOutput(`We will now attempt to reimport them with no timeout set.\n`);
		for (var x in retryPageImports) {
			var importcmd = retryPageImports[x];
			jobDebugOutput(importcmd);
			importPool.exec('shellCommand', [importcmd])
				.then((data) => {
					if (data.error) {
						jobErrorOutput('An error occured on page ' + data.fcount + '/' + data.ftotal + ' for retry.');
						fatalDontDelete = true;
					} else {
						jobOutput('Done with import ' + data.fcount + '/' + data.ftotal + ' for retry');
						otherScriptDebugOutput(data.output);
					}
					checkIfPostChunksDone(cb);
				})
				.catch((err) => {
					jobErrorOutput(`We've encountered a complete failure.`);
					jobErrorOutput(JSON.stringify(err));
					checkIfPostChunksDone(cb);
				});
		}
	} else {
		cb();
	}
}

var checkIfPostChunksDone = (cb) => {
	postImportCount++;
	status['Retrying Imports']--;
	if (postImportCount >= retryPageImports.length) {
		cb();
	}
};

var fireProcessComplete = () => {
	postImportErrorCheck(() => {
		sanityCheck(() => {
			runPostScripts(() => {
				purgeDump(() => {
					jobOutput('Process COMPLETELY DONE now.');
					jobOutput("\n\n");
					console.timeEnd("ShopVac Process");
					jobOutput("\n\n");

					if (discoveredExtensions.length) {
						var discovered = discoveredExtensions.filter((elem, pos) => {
							return discoveredExtensions.indexOf(elem) == pos;
						});
						var text = "Please consider enabling the following extensions: " + discovered.join(", ");
						text = ansi.format(text, 'cyan');
						jobOutput(text + "\n\n");
					}

					process.exit(0);
				});
			});
		});
	});
}

/***
 *       _____            _             _     ______                       _
 *      / ____|          | |           | |   |  ____|                     | |
 *     | |     ___  _ __ | |_ ___ _ __ | |_  | |__  __  ___ __   ___  _ __| |_
 *     | |    / _ \| '_ \| __/ _ \ '_ \| __| |  __| \ \/ / '_ \ / _ \| '__| __|
 *     | |___| (_) | | | | ||  __/ | | | |_  | |____ >  <| |_) | (_) | |  | |_
 *      \_____\___/|_| |_|\__\___|_| |_|\__| |______/_/\_\ .__/ \___/|_|   \__|
 *                                                       | |
 *                                                       |_|
 */

var exportContent = () => {
	gatherNS(handleNS);
}

/**
 * Wrapper for request to throw error if we dont have JSON.
 * @param {*} opts
 * @param {*} cb
 */
var jsonRequest = (opts, callback) => {
	opts.url = opts.url ? opts.url : opts.uri;
	importPool.exec('jsonRequest', [opts])
		.then((data) => {
			callback(data.err, data.data)
		})
		.catch((err) => {
			callback(err,[]);
		});
}

/**
 * gather namespace data from wikia for use in the entire export process.
 * will attempt to call out to wikia multiple times before failing.
 * @param function callback
 */
var gatherNS = (cb) => {
	var srcWikiNamespaceUrl = opt.urlProtocol + "://" + opt.srcWiki + "/api.php?action=query&meta=siteinfo&siprop=namespaces&format=json";
	jsonRequest({ method: "GET", url: srcWikiNamespaceUrl}, (err, data) => {
		if (err) {
			hardFail("Unable to get NS info from Source Wiki api");
		} else {
			cb(data);
		}
	});
};

var handleNS = (body) => {
	jobOutput('Namespaces pulled from Wiki:');
	var pNS = [];
	for (var x in body.query.namespaces) {
		var ns = body.query.namespaces[x];
		if (_.contains(options.export.ignoreNS, ns.id)) {
			/* Ignore namespaces that were specificly passed as ignore */
			jobOutput(" - " + ns['*'] + " [" + ns.id + "]: " + ansi.format('Skipping', 'red'));
		} else if (ns.id < 0) {
			jobOutput(" - " + ns['*'] + " [" + ns.id + "]: " + ansi.format('Not Scrapable', 'yellow'));
		} else {
			/* export this namespace */
			if (options.export.onlyNS.length) {
				if (_.contains(options.export.onlyNS, ns.id)) {
					jobOutput(" - " + ns['*'] + " [" + ns.id + "]: " + ansi.format('Scraping', 'green'));
					pNS.push(ns.id);
				} else {
					jobOutput(" - " + ns['*'] + " [" + ns.id + "]: " + ansi.format('Skipping', 'red'));
				}
			} else {
				jobOutput(" - " + ns['*'] + " [" + ns.id + "]: " + ansi.format('Scraping', 'green'));
				pNS.push(ns.id);
			}
		}
	}
	gc();
	runProcess(pNS);

}

var nstitles = [];

/**
 * Pulls titles form API recursivly.
 * @param {*} callback
 * @param {*} aifrom
 */
var getTitlesFromAPI = (ns, finished, aifrom) => {
	if (!aifrom) aifrom = "";
	if (typeof nstitles[ns] == "undefined") {
		nstitles[ns] = [];
	}
	aifrom = encodeURIComponent(aifrom);
	var url = opt.urlProtocol + "://" + opt.srcWiki + "/api.php?action=query&list=allpages&approp=title&apfrom=" + aifrom + "&apnamespace="+ns+"&aplimit="+options.export.titlesPerRequest+"&format=json"
	jobDebugOutput(url);


	jsonRequest({ method: "GET", url: url}, (err, body) => {
		if (err) {
			hardFail("Unable to get Images from api");
		} else {
			if (body.query && body.query.allpages) {
				jobOutput('Adding ' + body.query.allpages.length + ' titles to export queue for ns '+ns+'...');
				for (var x in body.query.allpages) {
					nstitles[ns].push(body.query.allpages[x]);
				}
				jobDebugOutput(body);
				gc();
			} else {
				jobOutput('Warning: Malformed API reponse?');
				jobOutput(body);
				finished(ns);
				gc();
			}
			if (body['query-continue']) {
				// MW 1.19 API - Wikia Stuff
				if (body['query-continue'].allpages && body['query-continue'].allpages.apfrom) {
					jobOutput('Paging exists, moving to aifrom: ' + body['query-continue'].allpages.apfrom);
					getTitlesFromAPI(ns, finished, body['query-continue'].allpages.apfrom);
					gc();
				} else {
					finished(ns);
					gc();
				}
			} else if (body['continue'] && body['continue'].apcontinue) {
				// Modern MW API
				jobOutput('Paging exists, moving to aifrom: ' + body['continue'].apcontinue);
				getTitlesFromAPI(ns, finished, body['continue'].apcontinue);
				gc();
			} else {
				// No paging?
				finished(ns);
				gc();
			}
		}
	});
};

/**
 * Get export from API
 * @param {*} callback
 * @param {*} aifrom
 */
var getExportFromAPI = (pageid, finished) => {
	var url = opt.urlProtocol + "://" + opt.srcWiki + "/api.php?action=query&pageids=" + pageid + "&format=json&export&prop=revisions&rvlimit=max&rvprop=ids|roles|flags|timestamp|user|userid|size|sha1|contentmodel|comment|parsedcomment|content|tags&rvslots=main"
	jobDebugOutput(url);
	jsonRequest({ method: "GET", url: url}, (err, body) => {
		if (err) {
			hardFail("Unable to get NS info from Source Wiki api");
		} else {
			if (body.query && body.query.export && body.query.export['*']) {
				parseString(body.query.export['*'], (err, result) => {
					if (result.mediawiki.page) {
						var newRevs = [];
						for (var x in body.query.pages[pageid].revisions) {
							var r = body.query.pages[pageid].revisions[x];
							var pushRev = true;

							if (options.export.minRevisionDate || options.export.maxRevisionDate) {
								var revDate = moment(r.timestamp);
								if ((options.export.maxRevisionDate && revDate.isAfter(options.export.maxRevisionDate.trim())) || (options.export.minRevisionDate && revDate.isBefore(options.export.minRevisionDate.trim()))) {
									// Revision is after our max evision date
									pushRev = false;
								}
							}

							if (pushRev) {
								newRevs.push({
									id: [ r.revid ],
									parentid: [ r.parentid ],
									timestamp: [ r.timestamp ],
									contributor: [{
											username: [ r.user ]
										}],
									comment: [ r.comment ],
									text: [{
										_: r['*'],
										'$': {
												'xml:space': 'preserve',
												bytes: r.size
											}
									}],
									sha1: r.sha1
								});
							}
						}

						result.mediawiki.page[0].revision = newRevs;
						// only proceed with process if any revisions for this page exist.
						if (newRevs.length) {
							try {
								var builder = new xmlBuilder();
								var modifiedXML = builder.buildObject(result);
							} catch (e) {
								// if there is a failure, just revert back to original content
								var modifiedXML = body.query.export['*'];
							}
							finished(pageid, modifiedXML);
							gc();
						}
					} else {
						finished(pageid, body.query.export['*']);
						gc();
					}
				})
			} else {
				jobOutput('Warning: Malformed API reponse?');
				jobOutput(body);
				gc();
			}
		}
	});
};

/**
 * Process an XML string to clean it up for our import.
 * @param {*} xml
 */
var processExport = (pageid, xml, cb) => {
	iwd++;
	getSiteData('src',(srcData) => {
		getSiteData('dest',(destData)=> {

			jobDebugXMLOutput(ansi.format("\n\n===== PRE PROCESSED XML FOR PAGE " + pageid + " =====\n\n", 'cyan') + xml);
			jobDebugXMLOutput(ansi.format("\n\n===== Operation Running =====\n\n", 'cyan'))

			var ogSitename = false;
			var newSitename = false;
			if (srcData.sitename == srcData.mainpage) {
				// source data matches. We need to try and be a little fancy.
				ogSitename = destData.sitename; // save this for use later in restoring some removals.
				destData.sitename = destData.sitename.split(":").join("").split("'").join("");//.split("&").join("---AND---");
				newSitename = destData.sitename;

				jobDebugXMLOutput("Renaming destination sitename from "+ogSitename+" to "+newSitename);
			}

			jobDebugXMLOutput(srcData.sitename + " => " + destData.sitename);
			jobDebugXMLOutput(srcData.sitename.split(" ").join("_") + " => " + destData.sitename.split(" ").join("_"));
			jobDebugXMLOutput(srcData.mainpage + " => " + destData.mainpage);
			jobDebugXMLOutput(srcData.mainpage.split(" ").join("_") + " => " + destData.mainpage.split(" ").join("_"));

			xml = xml
			// Replace site name
			.split(srcData.sitename).join(destData.sitename)
			// Replace sitename with underscores instead of spaces.
			.split( srcData.sitename.split(" ").join("_") ).join( destData.sitename.split(" ").join("_") )
			// replace mainpage name
			.split(srcData.mainpage).join(destData.mainpage)
			// Replace mainpage with underscores instead of spaces.
			.split(srcData.mainpage.split(" ").join("_")).join(destData.mainpage.split(" ").join("_"))
			// Wikia to Gamepedia Naming Fixes
			.split(opt.srcWiki).join(opt.destWiki) // Replace wiki urls
			.split('wikia.com').join('gamepedia.com')
			.split('wikia').join('wiki') // Replace wikia with wiki
			.split('Wikia').join('Wiki') // Replace Wikia with Wiki
			.split('gamepedia.com/wiki/').join('gamepedia.com/') // Trim that /wiki/ garbage off the tail end.
			// Page Name Cleanups
			.split(':Community Portal').join(':Community portal')
			// File name fixes
			.split(".PNG").join(".png")
			.split(".JPG").join(".jpg")
			.split(".GIF").join(".gif")
			; // this simicolon is cold and lonely.

			// make sure all & are properly escaped in the xml, hopefully.
			xml = xml.split("& ").join("&amp; ").split("&_").join("&amp;_");

			parseString(xml, function (err, result) {
				if (err) {
					jobErrorOutput(err);
					jobErrorOutput(this);
				} else if (!result.mediawiki) {
					jobErrorOutput("Failed to parse as a mediawiki import: ");
					jobErrorOutput(result);
					jobErrorOutput(this);
				} else {
					try {
						if (ogSitename && newSitename) {
							// its easier to put back the things we broke then try and find all the places it shouldn't break
							// using lazy try catch structures in lue of actual checking
							try {
								jobDebugXMLOutput("Settings sitename back to "+ogSitename);
								result.mediawiki.siteinfo[0].sitename = ogSitename;
							} catch (e) {}
							try {
								// Make sure the mainpage is correct
								if (result.mediawiki.page[0].title[0] == "MediaWiki:Mainpage") {
									jobDebugXMLOutput("Forcing Mainpage to correct mainpage.");
									result.mediawiki.page[0].revision[0].text[0]._ = destData.sitename.split(" ").join("_");
								}
							} catch (e) {}
						}

						if (options.export.purgeRevisions) {
							var backupRev = result.mediawiki.page[0].revision[0];
							result.mediawiki.page[0].revision = [backupRev]; // Lets make sure we wipe away multiple revisions if they exist.

							delete result.mediawiki.page[0].revision[0].contributor[0].ip; // no ip
							delete result.mediawiki.page[0].revision[0].contributor[0].id; // no id
							result.mediawiki.page[0].revision[0].contributor[0].username = "Maintenance_script";
							result.mediawiki.page[0].revision[0].timestamp = "";
							result.mediawiki.page[0].revision[0].comment = "Automated Import";
						}
						// Convert Mediawiki:Wiki-navigation to Mediawiki:Sidebar.
						if (result.mediawiki.page[0] == "MediaWiki:Wiki-navigation") {
							result.mediawiki.page[0].title[0] = "MediaWiki:Sidebar";
							xml = xml.replace("<title>MediaWiki:Wiki-navigation</title>", "<title>MediaWiki:Sidebar</title>");
						}

						var builder = new xmlBuilder();
						var xml = builder.buildObject(result);

						// Replace slideshows with SlideBoxLightShow.
						if (result.mediawiki.page[0].revision[0].text[0]._ && minimatch(result.mediawiki.page[0].revision[0].text[0]._, "<gallery type=\"slideshow\">*</gallery>")) {
							var startTag = '<gallery type="slideshow">';
							var endTag = '</gallery>';

							// split by gallery tag
							var split = xml.split(startTag);
							for (var x in split) {
								// replace first instance of an END gallery tag.
								split[x] = split[x].replace(endTag, "</slideboxlightshow>");
							}
							// join back together with slideboxlightshow tags.
							xml = split.join('<slideboxlightshow>');
						}
					} catch (e) {
						// Dont use jobErrorOutput for things that don't work
						jobErrorOutput(e);
						var builder = new xmlBuilder();
						var xml = builder.buildObject(result);
						jobErrorOutput("This is the offending XML: ")
						jobErrorOutput(xml);
					}

					jobDebugXMLOutput(ansi.format("\n\n===== POST PROCESSED XML FOR PAGE " + pageid + " =====\n\n", 'cyan') + xml);

					if (options.dev.testRunOnly) {
						jobOutput('Not importing because --testRunOnly');
					} else {
						iwd--;
						cb(pageid, xml);
					}
					gc();
				}
			}.bind(xml));
		});
	});
}

var retryPageImports = [];
var importedUsers = [];

var importFromXML = (pageid, xml, cb) => {
	iwd++;
	// Before actually importing, lets create users to stop MediaWiki from doing it in a dumb way.

	// User regex to extract usernames from XML instead of parsing the whole thing into an object again
	var extractUsers = xml.match(/<username>(.*?)<\/username>/g).map(function(val){
		return val.replace(/<\/?username>/g,'');
	});

	// Remove duplicates from the list.
	extractUsers = extractUsers.sort().filter((item, pos, ary) => {
        return !pos || item != ary[pos - 1];
	});

	// Remove IP addresses from the list
	extractUsers = extractUsers.filter((item, pos, ary) => {
		return !/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(item);
	});
	// Handle IPv6 =P
	extractUsers = extractUsers.filter((item, pos, ary) => {
		return !item.includes(":");
	});

	//  Filter and manage already imported users
	for (var x in extractUsers) {
		var item = extractUsers[x];
		if (importedUsers.indexOf(item) !== -1) {
			delete(extractUsers[x]);
		} else {
			importedUsers.push(item);
		}
	}

	// remove empty rows
	extractUsers = extractUsers.filter(function (el) {
		return el != null;
	});

	iwd--;
	if (extractUsers.length) {
		var newUsers = extractUsers.join(',');
		var newUserCmd = `php ${opt.hcMaintPath}/hostHelper.php ${opt.destWiki} ${opt.svMaintPath}/newUser.php '${newUsers}'`;
			importPool.exec('shellCommand', [newUserCmd, pageid, 600000,{xml:xml}])
			.then((data) => {
				jobDebugOutput(data);
				processXMLImport(data.pageid, data.extra.xml, cb);
				gc();
			});
	} else {
		// No new users found. Proceed.
		processXMLImport(pageid, xml, cb);
	}
}

var processXMLImport = (pageid, xml, cb) => {

	var fixedPath = opt.basePath + "/titles/"+pageid+".xml";
	fs.writeFile(fixedPath, xml, (err) => {
		if (err) {
			status['Failed Titles']++;
			cb();
		} else {
			var importcmd = `php ${opt.hcMaintPath}/hostHelper.php ${opt.destWiki} ${opt.maintPath}/importDump.php --username-prefix="" ${fixedPath}`;
			importPool.exec('shellCommand', [importcmd, pageid, 600000])
			.then((data) => {
				if (data.error) {
					jobErrorOutput('An error occurred on import for pageid ' + data.pageid);
					jobDebugOutput(data.error);
					otherScriptDebugOutput(data.output);
					retryPageImports.push(data.importcmd);
					status['Failed Titles']++;
					cb();
				} else {
					jobOutput('Done with import for pageid '+data.pageid);
					otherScriptDebugOutput(data.output);
					fs.removeSync(fixedPath);
					cb();
				}

			})
			.catch((err) => {
				//jobErrorOutput(`An error occured on an import that we can't recover from. Sad face emoji.`);
				//jobErrorOutput(err);
				//status['Failed Titles']++;
				//process.exit();
			});
		}
	});
}

var exportAndImportTitle = (data, cb) => {
	var pageid = data.pageid;
	getExportFromAPI(pageid, (pageid, xml) => {
		processExport(pageid, xml, (pageid, xml)=>{
			importFromXML(pageid, xml,()=>{
				cb();
			})
		});
	})
}

var q2 = [];
var donetitles = [];
var exportNS = (ns, cb, retry) => {
	status['Scraping Namespaces']++;
	statusHasChanges = true; // fight race condition for slow spinups.
	jobOutput("Starting " + ansi.format('export', 'cyan') + " for Namespace " + ns);
	//var path = opt.basePath + '/ns-' + ns;
	var logPath = opt.basePath + '/logs/ns-' + ns + '.log';

	if (options.export.skipXMLDump) {
		status['Scraping Namespaces']--;
		donetitles.push(ns);
		cb(ns);
	} else {
		getTitlesFromAPI(ns,(ns)=>{
			var titles = nstitles[ns];

			if (!titles.length && donetitles.indexOf(ns) === -1) {
				donetitles.push(ns);
				status['Scraping Namespaces']--;
				status['Failed Namespaces']++;
				jobErrorOutput("No titles for namespace "+ ns + "...");
			}

			status['Total Titles'] += titles.length;
			// create queue for processing
			q2[ns] = new Queue(exportAndImportTitle, {
				batchSize: 1,
				concurrent: options.export.maxRevisionDate
			});

			q2[ns].on('task_finish', function (taskId, result, stats) {
				status['Completed Titles']++;
			});
			q2[ns].on('task_failed', function (taskId, err, stats) {
				status['Failed Titles']++;
				jobErrorOutput(err.toString());
			});
			q2[ns].on('drain', function () {
				jobOutput('Move of Namespace ' + ns + ' completed!');
				donetitles.push(ns);
				status['Scraping Namespaces']--;
				handleMainStatus();
				cb(ns);
			});

			for (var x in titles) {
				q2[ns].push({ pageid: titles[x].pageid });
			}

		});
	}

};

var siteData = {
	src: null,
	dest: null
};

var getSiteData = (from, cb) => {
	switch (from) {
		case 'src':
		default:
			domain = opt.srcWiki;
			from = 'src'; // its source if its not destination
		break;
		case 'dest':
			from = 'dest';
			domain = opt.destWiki;
		break;
	}

	//jobDebugOutput("Call to get info for " + domain);
	if (siteData[from]) {
		//jobDebugOutput(siteData[from]);
		return cb(siteData[from]);
	} else {
		var url = opt.urlProtocol + "://" + domain + "/api.php?action=query&meta=siteinfo&siprop=general&format=json&formatversion=2"
		jobDebugOutput(url);

		jsonRequest({ method: "GET", url: url}, (err, data) => {
			if (err) {
				hardFail("Unable to get site data");
			} else {
				siteData[from] = data.query.general;
				cb(data.query.general);
			}
		});
	}
}

var completeNS = (ns) => {
	jobOutput('export and Import of Namespace ' + ns + ' completed!');
	status['Completed Namespaces']++;
	handleMainStatus();
}

/**
 * 	Actual Process Code that does the work.
 */
var runProcess = (namespaces) => {
	status['Total Namespaces'] = namespaces.length;
	for (var ns in namespaces) {
		exportNS(namespaces[ns], (ns) => {
			completeNS(ns);
		});
	}
};

/***
 *      _____                              ______                       _
 *     |_   _|                            |  ____|                     | |
 *       | |  _ __ ___   __ _  __ _  ___  | |__  __  ___ __   ___  _ __| |_
 *       | | | '_ ` _ \ / _` |/ _` |/ _ \ |  __| \ \/ / '_ \ / _ \| '__| __|
 *      _| |_| | | | | | (_| | (_| |  __/ | |____ >  <| |_) | (_) | |  | |_
 *     |_____|_| |_| |_|\__,_|\__, |\___| |______/_/\_\ .__/ \___/|_|   \__|
 *                             __/ |                  | |
 *                            |___/                   |_|
 */

var images = [];

/**
 * Pulls images form API recursivly.
 * @param {*} callback
 * @param {*} aifrom
 */
var getImagesFromAPI = (finished, aifrom) => {
	var imagePerCall = 500;
	if (!aifrom) aifrom = "";
	aifrom = encodeURIComponent(aifrom);
	var url = opt.urlProtocol + "://" + opt.srcWiki + "/api.php?action=query&list=allimages&aiprop=url&aifrom=" + aifrom + "&ailimit="+imagePerCall+"&format=json"
	jobDebugOutput(url);

	jsonRequest({ method: "GET", url: url}, (err, body) => {
		if (err) {
			hardFail("Unable to get site data");
		} else {
			if (body.query && body.query.allimages) {
				jobOutput('Adding ' + body.query.allimages.length + ' images...');
				for (var x in body.query.allimages) {
					images.push(body.query.allimages[x]);
				}
			} else {
				jobOutput('Warning: Malformed API reponse?');
				jobOutput(body);
			}

			if (body['query-continue']) {
				// MW 1.19 API - Wikia Stuff
				if (body['query-continue'].allimages && body['query-continue'].allimages.aifrom) {
					jobOutput('Paging exists, moving to aifrom: ' + body['query-continue'].allimages.aifrom);
					getImagesFromAPI(finished, body['query-continue'].allimages.aifrom);
				} else {
					finished();
				}
			} else if (body['continue'] && body['continue'].aicontinue) {
				// Modern MW API
				jobOutput('Paging exists, moving to aifrom: ' + body['continue'].aicontinue);
				getImagesFromAPI(finished, body['continue'].aicontinue);
			} else {
				// No paging?
				finished();
			}
		}
	});
};

var failedImages = [];
var processImages = () => {
	status['Image Status'] = "Starting Up...";
	statusHasChanges = true;
	getImagesFromAPI(() => {
		status['Total Images'] = images.length;
		status['Processed Images'] = 0;
		status['Image Status'] = "Processing";
		for (var x in images) {
			var name = images[x].name;
			name = name.split('"').join('\\"');
			name = name.replace(".PNG", ".png");
			name = name.replace(".JPG", ".jpg");
			name = name.replace(".GIF", ".gif");
			importPool.exec('importImage', [{
				name: name,
				url: images[x].url
			}, {
				basePath: opt.basePath,
				maintPath: opt.maintPath,
				hcMaintPath: opt.hcMaintPath,
				destWiki: opt.destWiki,
				remoteImageApi: opt.remoteImageApi
			}]).then((data) => {
				if (data) {
					if (data.failedImages) {
						failedImages.push(data.failedImages);
						status['Failed Images']++;
					} else {
						status['Complete Images']++;
					}
					jobOutput(data.output);
				}
			}).catch((err) => {
				jobErrorOutput(err);
			}).then(()=>{
				status['Processed Images']++;
				if (status['Total Images'] == status['Processed Images']) {
					status['Image Status'] = "Complete";
					if (failedImages.length) {
						jobErrorOutput("\n\n\nThe following images failed: ");
						for(var x in failedImages) {
							jobErrorOutput(" -- " + failedImages[x]);
						}
					}
					handleMainStatus();
				}
			});
		}
	});
}

/***
 *      _____                               ______                     _   _
 *     |  __ \                             |  ____|                   | | (_)
 *     | |__) | __ ___   ___ ___  ___ ___  | |__  __  _____  ___ _   _| |_ _  ___  _ __
 *     |  ___/ '__/ _ \ / __/ _ \/ __/ __| |  __| \ \/ / _ \/ __| | | | __| |/ _ \| '_ \
 *     | |   | | | (_) | (_|  __/\__ \__ \ | |____ >  <  __/ (__| |_| | |_| | (_) | | | |
 *     |_|   |_|  \___/ \___\___||___/___/ |______/_/\_\___|\___|\__,_|\__|_|\___/|_| |_|
 *
 *
 */


/*
	Make sure *some* content comes in before
	all images are completed, so that
	_certain people_ named carol don't
	just assume its broken when no content is flowing in...
*/

setInterval(()=>{
	jobOutput('Restarting worker pool to avoid any potential memory leaks');
	importPool.terminate();
},120000);


let preflight = (done) => {
	jobOutput("Preflight Checks Running...");
	jobOutput('Checking Environment:');
	jobOutput('  + NodeJS Version:		' + process.version);
	var mainNodeVersion = process.versions.node.split(".")[0];
	if (mainNodeVersion < 6) {
		jobOutput(ansi.format('   - Only tested with version 6+. Consider Upgrading.', 'yellow'));
	}
	getSiteData('src', (srcData) => {
		if (typeof srcData !== "object") {
			throw Error("Unable to preflight check the source wiki.");
		}
		jobOutput("	- Source Wiki Appears Valid.")
		jobDebugOutput(srcData);
		getSiteData('dest', (destData) => {
			if (typeof destData !== "object") {
				throw Error("Unable to preflight check the destination wiki.");
			}
			jobOutput("	- Destination Wiki Appears Valid.")
			jobDebugOutput(destData);
			// Preflight Checks Done

			done();
		});
	});
 }

preflight(()=>{
	if (!options.export.skipContent) {
		jobOutput("\nStarting export Process.\n\n")
		exportContent();
	}
	if (!options.export.skipImages) {
		processImages();
	}
});
