# ShopVac
A tool for importing / migrating wikis from outside sources using the MediaWiki API.

> This extension relies heavily on [DynamicSettings](https://gitlab.com/hydrawiki/extensions/dynamicsettings) and [SyncService](https://gitlab.com/hydrawiki/extensions/syncservice) will not work as provided outside of a Hydra stack.

## History
Histrocially, anytime we wanted to bring a new wiki into our farm, it was a long time consuming process to get quality exports of content & images and bring them into the Hydra stack. It could often take up a day or two of a developers time -- Something not remotely scalable. 

Being developers, we can only do something manually so many times before the itch to automate the process takes over. At first, there was a collection of scripts that drasticly reduces the manual burden, but we were still handling these migrations on our local development machines. Eventually, we realized with a bit of work, we could build an extension that would allow migrations to happen on the stack without any manual labor from the development team -- thus ShopVac was born.

## How does it work?
All this extension does is provide a GUI for running the ShopVac script through Sync Service.

The process of the script is outlined below. All processes are done asynconously to increase speed of the process, tho this can be taxing on the machine running it.

### For Content
Content export flow is essentially as follow:
- Accepts a list of namespaces to process (this allows specific content to be excluded if needed)
- For each namespace, it gets page exports 1 at a time from the remote wiki's api.
- Post process this content export to make sure it will cleanly import into our stack
   - An example of this would be replacing titles that were specific to an alternate wiki farm.
- Save this export out to disk, and use MediaWiki's built in importXML script to import.
- Delete export from disk

### For Images
Images are processed in a similar way, but images are handled using the remote site's api endpoint for "listing all media". 
- Get all media from api
- Download image into its own folder
- Run mediawiki's importImages script on that single folder
- Delete image (and its folder) form disk