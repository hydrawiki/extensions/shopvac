$(function () {

	var api = new mw.Api();

	var options = {
		'srcWiki': '',
		'deskWiki': '',
		'skipContent': false,
		'skipImages': false,
		'onlyNS': [],
		'siteKey': '',
		'useHTTP': false,
		'hideNSSelector': false,
		'importContentWithImages': true,
		'minRevisionDate': false,
		'maxRevisionDate': false
	};

	function handleOptionChange() {
		// handle generation of scraper args
		/*
		--srcWiki miley-cyrus.wikia.com
		--destWiki shopvac.gamepedia.com
		--skipContent
		--skipImages
		--onlyNS 1,2,3,4
		*/

		var args = [];
		console.log('Generating from data', options);

		var jobWillRun = true;
		var reason = '';

		if(options.hideNSSelector) {
			$("#namespaceSelector").hide();
		} else {
			$("#namespaceSelector").show();
		}

		if (options.siteKey) {
			$("#siteKey").val(options.siteKey);
		} else {
			$("#siteKey").val('');
			jobWillRun = false;
			reason = mw.message('reasonunabletodeterminesitekeyfordestinationwiki').escaped();
		}

		if (options.srcWiki) {
			args.push("--srcWiki=" + options.srcWiki);
		} else {
			jobWillRun = false;
			reason = mw.message('reasonnosourcewikidefined').escaped();
		}
		if (options.destWiki) {
			args.push("--destWiki=" + options.destWiki);
		} else {
			jobWillRun = false;
			reason = mw.message('reasonnodestinationwikidefined').escaped();
		}
		if (options.skipContent) {
			args.push("--skipContent");
		}
		if (options.skipImages) {
			args.push("--skipImages");
		}

		if (options.minRevisionDate) {
			args.push("--minRevisionDate=\""+ options.minRevisionDate +"\"");
		}

		if (options.maxRevisionDate) {
			args.push("--maxRevisionDate=\""+ options.maxRevisionDate +"\"");
		}

		if (options.importContentWithImages) {
			args.push("--importImagesUsingSourceAPI");
			// remove the file namespace from the scrape
			var sixpos = options.onlyNS.indexOf('6');
			if (sixpos !== -1) {

				options.onlyNS.splice(sixpos, 1);
			}
		}

		if (options.onlyNS.length) {
			var nsssv = options.onlyNS.join(" ");
			args.push("--onlyNS " + nsssv);
		} else {
			// If no NS selected, we don't want it to scrape any (even tho this is dumb);
			if (!options.skipContent) {
				args.push("--skipContent");
			}
		}

		if (options.useHTTP) {
			args.push("--useHTTP");
		}

		var argString = args.join(" ");
		$("#scraperArgs").val(argString);

		if (jobWillRun) {
			$("#runBtn").prop("disabled", false);
			$("#runMsg").html("");
		} else {
			$("#runBtn").prop("disabled", true);
			$("#runMsg").html(reason);
		}

	}

	var validateDate = function (isoDate) {

		if (isNaN(Date.parse(isoDate))) {
			return false;
		} else {
			if (isoDate != (new Date(isoDate)).toISOString().substr(0,10)) {
				return false;
			}
		}
		return true;
	}

	var handleNamespaceChange = function() {
		var scrapeNS = [];
		$(".nscheck").each(function () {
			if (this.checked) {
				var id = $(this).attr('data-id');
				scrapeNS.push(id.toString());
			}
		});
		options.onlyNS = scrapeNS;
		handleOptionChange();
	}

	// handler for changing NS
	$(document).on('click', ".nscheck", function () {
		handleNamespaceChange();
	});

	var handleCollectData = function (value) {
		switch (value) {
			case 'both':
				options.skipContent = false;
				options.skipImages = false;
				options.hideNSSelector = false;
				options.importContentWithImages = true;
				handleNamespaceChange(); // repopulate onlyNS config from UI.
				break;
			case 'images':
				options.skipContent = true;
				options.skipImages = false;
				options.importContentWithImages = true;
				options.onlyNS = [];
				options.hideNSSelector = true;
				break;
			case 'images_only':
				options.skipContent = true;
				options.skipImages = false;
				options.importContentWithImages = false;
				options.onlyNS = [];
				options.hideNSSelector = true;
				break;
			case 'content':
				options.skipContent = false;
				options.skipImages = true;
				options.hideNSSelector = false;
				options.importContentWithImages = false;
				handleNamespaceChange(); // repopulate onlyNS config from UI.
				break;
		}
		handleOptionChange();
	}

	// handler for changing scrape data type
	$('input[type=radio][name=collectData]').change(function () {
		handleCollectData(this.value);
	});

	handleCollectData($('input[type=radio][name=collectData]').val());

	$("#minRevision").keyup(function (e) {
		var val = $("#minRevision").val();
		if (validateDate(val)) {
			options.minRevisionDate = val;
		} else {
			options.minRevisionDate = false;
		}
		handleOptionChange();
	});

	$("#maxRevision").keyup(function (e) {
		var val = $("#maxRevision").val();
		if (validateDate(val)) {
			options.maxRevisionDate = val;
		} else {
			options.maxRevisionDate = false;
		}
		handleOptionChange();
	});

	// keep line breaks out of the text area.
	$("#scraperArgs").keypress(function (e) {
		if (e.keyCode != 13) return;
		var content = $("#scraperArgs").val().replace(/\n/g, "");
		$("#scraperArgs").val(content);
		return false;
	});


	var pfSrc = function (add, callback) {
		var domain = $("#srcWiki").val();
		var adders = [
			'',
			'/w'
		];
		if (typeof adders[add] == 'undefined') {
			$("#preflightStatus").append("\n"+mw.message('failedtofindapiforsource').escaped());
			return;
		}
		domain = domain + adders[add];
		console.log('Checking ' + domain);
		api.get({
			action: 'shopvac',
			do: 'getWikiInfoSrc',
			formatversion: 2,
			domain: domain
		}).fail(function (xhr, status) {
			if (typeof adders[add + 1] == 'undefined') {
				var pretty = JSON.stringify(status, null, 2);
				$("#preflightStatus").append("\n" + mw.message('preflighterror_source').escaped()+"\n\n" + pretty);
			}
			pfSrc(add + 1, callback);
		}).done(function (data) {
			callback(data, domain);
		});
	};


	$("#preflightRun").click(function () {
		$(".disableForPreflight input, .disableForPreflight button").each(function () {
			$(this).prop("disabled", true);
		});
		var srcWiki = $("#srcWiki").val().replace("http://", "").replace("https://", "");
		var destWiki = $("#destWiki").val().replace("http://", "").replace("https://", "");
		$("#srcWiki").val(srcWiki);
		$("#destWiki").val(destWiki);
		$("#preflightStatus").html(mw.message('preflightrunning').escaped());
		pfSrc(0, function (dataSrc, srcWiki) {
			console.log(dataSrc);
			$("#preflightStatus").append("\n\n"+mw.message('gotsrc').escaped());
			if (typeof dataSrc.error !== "undefined" || dataSrc.success !== true || dataSrc.data === null) {
				var pretty = JSON.stringify(dataSrc, null, 2);
				$("#preflightStatus").append("\n" + mw.message('preflighterror_source').escaped()+"\n\n" + pretty);
			} else {
				api.get({
					action: 'shopvac',
					do: 'getWikiInfoDest',
					formatversion: 2,
					domain: destWiki
				}).fail(function (xhr, status, error) {
					var pretty = JSON.stringify(status, null, 2);
					$("#preflightStatus").append("\n" + mw.message('preflighterror_dest').escaped() + "\n\n" + pretty);
				}).done(function (dataDest) {
					$("#preflightStatus").append("\n\nGot dest wiki information.");
					console.log(dataDest);
					if (typeof dataDest.error !== "undefined" || dataSrc.success !== true || dataSrc.data === null) {
						var pretty = JSON.stringify(dataDest, null, 2);
						$("#preflightStatus").append("\n" + mw.message('preflighterror_dest').escaped() + "\n\n" + pretty);
					} else {
						/*
							No errors. We should be able to continue.
						*/
						options.srcWiki = srcWiki; // set it if the return is good.
						options.destWiki = destWiki; // set it if the return is good.

						$("#srcWikiPanel input").each(function () {
							$(this).prop("disabled", false);
						});

						var wikiSrc = dataSrc.data;

						if (wikiSrc.useHTTP) {
							options.useHTTP = true;
						}

						var nsdata = $.map(wikiSrc.namespaces, function (value, index) {
							return [value];
						});

						// Output basic wiki info (good to verify we are scraping what we actually want)
						var output = mw.message('wikiname').escaped() + ":  " + wikiSrc.sitename + "\n"
							+ mw.message('wikibase').escaped() + ":  " + wikiSrc.base + "\n"
							+ mw.message('namespaces').escaped() + ": " + nsdata.length + "\n"
							+ mw.message('usehttps').escaped() + ":  " + (wikiSrc.useHTTP ? "	&#x1F44E;" : "&#128077;") + "\n\n"
							+ mw.message('ifinfomatchinScrape').escaped();
						$("#srcWikiJSON").html(output);

						// @todo: load this from a config
						var ignoreNS = [1, 110, 111, 116, 117, 500, 501, 502, 503, 1200, 1201, 2000, 2001];
						// build namespace selectors
						$("#namespaceCheckboxes").html("");
						var selectAll = "<button id=\"select-all-ns\">" + mw.message('selectall').escaped() + "</button><button id=\"select-none-ns\">" + mw.message('deselectall').escaped() +"</button><br />";
						$("#namespaceCheckboxes").append(selectAll);

						$(document).on('click', '#select-all-ns', function () {
							$('.nscheck').each(function () {
								$(this).prop('checked', true);
							});
							handleNamespaceChange();
						});

						$(document).on('click', '#select-none-ns', function () {
							$('.nscheck').each(function () {
								$(this).prop('checked', false);
							});
							handleNamespaceChange();
						});

						for (var x in nsdata) {
							var ns = nsdata[x];
							if (typeof ns.id !== "undefined") {
								var extra = "";
								if ($.inArray(ns.id, ignoreNS)) {
									options.onlyNS.push(ns.id.toString());
									extra = " checked=\"checked\"";
								}
								var build = "<input type=\"checkbox\" class=\"nscheck\" name=\"scrape-" + ns.id + "\" data-id=\"" + ns.id + "\"" + extra + "><label for\"scrape-" + ns.id + "\">[" + ns.id + "] " + ns.canonical + "</label><br />";
								$("#namespaceCheckboxes").append(build);
							}
						}

						// destination wiki preflight

						var wikiDest = dataDest.data;

						var output = mw.message('wikiname').escaped() +  ":    " + wikiDest.name + "\n"
							+ mw.message('wikidomain').escaped() + ":  " + wikiDest.domain + "\n"
							+ mw.message('sitekey').escaped() + ":      " + wikiDest.sitekey + "\n\n"
							+ mw.message('ifinfomatchinImport').escaped();
						$("#destWikiJSON").html(output);

						options.siteKey = wikiDest.sitekey;

						$(".disableForPreflight input, .disableForPreflight button").each(function () {
							$(this).prop("disabled", false);
						});

						$(".disableForPreflight").show();
						$("#preflightPanel").hide();


						handleOptionChange();
					}
				});
			}
		});
	});
});
